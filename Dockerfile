FROM node:16 as build
WORKDIR /home/mocks/bin/api-config
COPY ./package.json ./
COPY ./package-lock.json* ./
RUN npm ci
COPY ./mocks ./mocks

FROM node:16-buster-slim
COPY --from=build /home/mocks/bin/api-config ./app

EXPOSE 4001
CMD node ./app/mocks/index.js
