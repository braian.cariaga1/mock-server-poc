# Api Config Mocks - PoC -

## How to run:

### Local

1. install: `npm i`
2. run: `npm start`

default port: `4001`

you can explore the ui in: `http://localhost:4001`.

### Docker

1. use a `Dockerfile` to build an image (in the root of this project): `docker build -t mocks/api-config`
2. create (and run) a container with the image `docker run --name api-config-mocks -p 4001:4001 -d mocks/api-config`

you can explore the ui in: `http://localhost:4001`.

## TODO

- Deploy to k8s
- Volume to store data
